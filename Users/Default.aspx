﻿<%@ Page 
    Language="C#" 
    AutoEventWireup="true"
     CodeBehind="Default.aspx.cs" 
    Inherits="MeetingManager.Users.Default"
     MasterPageFile="/Layouts/ForUsers.master"
     meta:resourcekey="PageResource1" %>

<asp:Content runat="server" ContentPlaceHolderID="MasterPageForUsers_Content">

    <h3 class="text-center" runat="server" id="TitleLabel" meta:resourcekey="TitleLabel"></h3>

    <asp:ObjectDataSource ID="usersDataSource" runat="server"
        TypeName="MeetingManager.Domain.CRUD.UsersRepository"
        DataObjectTypeName="MeetingManager.Domain.Entities.MeUser"
        StartRowIndexParameterName="start"
        SelectCountMethod="get_Count"
        MaximumRowsParameterName="max"
        EnablePaging="True"
        SelectMethod="Select"
        DeleteMethod="Delete"
        OnObjectCreating="usersDataSource_ObjectCreating" />

    <asp:GridView runat="server" ID="usersGridView"
        DataKeyNames="Id,UserName"
        DataSourceID="usersDataSource"
        AutoGenerateColumns="False"
        AllowPaging="True"
        PageSize="20"
        OnRowCommand="GridView_RowCommand"
        OnRowDeleting="GridView_Deleting"
        CssClass="table table-bordered table-hover td-last-child-200"
        BorderWidth="0px"
        EmptyDataText="No data found" meta:resourcekey="usersGridViewResource1">
        <Columns>
            <asp:BoundField HeaderText="Id" DataField="Id" meta:resourcekey="BoundFieldResource1" />
            <asp:BoundField HeaderText="Email" DataField="UserName" meta:resourcekey="BoundFieldResource2" />
            <asp:TemplateField HeaderText="Employee" meta:resourcekey="TemplateFieldResource1">
                <ItemTemplate>
                    <asp:Label runat="server" ID="employeeLabel" Text='<%# Eval("Employee.Name") %>' meta:resourcekey="employeeLabelResource1"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Roles" meta:resourcekey="TemplateFieldResource2">
                <ItemTemplate>
                    <asp:Label runat="server" Text="<%# MeetingManager.Domain.Extensions.MeUserExtensions.GetRoles(((MeetingManager.Domain.Entities.MeUser)((GridViewRow)Container).DataItem)) %>" meta:resourcekey="LabelResource2"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Command" meta:resourcekey="TemplateFieldResource3">
                <ItemTemplate>
                    <div class="btn-group-vertical btn-group-sm" role="group" aria-label="...">
                        <asp:Button CommandName="Select" CssClass="btn btn-primary" CommandArgument='<%# ((GridViewRow) Container).DataItemIndex %>' Text="<%$Resources:GridViewResource,Select %>" runat="server"></asp:Button>
                        <asp:Button CommandName="Edit" CssClass="btn btn-default" CommandArgument='<%# ((GridViewRow) Container).DataItemIndex %>' Text="<%$Resources:GridViewResource,Edit %>" runat="server"></asp:Button>
                        <asp:Button CommandName="Delete" CssClass="btn btn-danger" CommandArgument='<%# ((GridViewRow) Container).DataItemIndex %>' OnClientClick="if(!confirm(document.getElementById('deleteConfirm').value))return false;" Text="<%$Resources:GridViewResource,Delete %>" runat="server"></asp:Button>
                    </div>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>

    <script type="text/javascript">
        $("tr.pagination > td> table").addClass("table table-bordered table-hover");
        $("tr.pagination > td> table> tbody > tr > td > span").addClass("active");
        $("tr.pagination > td> table> tbody > tr > td > a").addClass("disabled");
    </script>

    <asp:PlaceHolder ID="contentPlace" runat="server" />

</asp:Content>
