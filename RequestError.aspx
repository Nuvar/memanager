﻿<%@ Page Title="" Language="C#" MasterPageFile="/Layouts/DefaultLayout.Master" AutoEventWireup="true" CodeBehind="RequestError.aspx.cs" Inherits="MeetingManager.RequestError" meta:resourcekey="PageResource1" %>

<asp:Content ID="Content3" ContentPlaceHolderID="Content" runat="server">
    <div class="row">
        <h3 runat="server" class="text-center"><%=GetLocalResourceObject("ErrorTitle") %></h3>
        <h4 runat="server" id="DetailsError" class="text-center"></h4>
    </div>
</asp:Content>
