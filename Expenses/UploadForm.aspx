﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UploadForm.aspx.cs" Inherits="MeetingManager.UploadExpensesForm" MasterPageFile="/Layouts/ForUsers.master" meta:resourcekey="PageResource1" %>

<asp:Content runat="server" ContentPlaceHolderID="MasterPageForUsers_Content" ClientIDMode="Static">
    <div class="row">
        <h3 class="text-center" runat="server" id="TitleLabel" meta:resourcekey="TitleLabel"></h3>
        <div class="form-group">
            <h4 class="text-center" runat="server" id="H1" meta:resourcekey="SelectLabel"></h4>
            <div class="form-group">
                <asp:FileUpload runat="server" ID="expensesUpload" data-show-preview="false" CssClass="file form-control" meta:resourcekey="expensesUploadResource1" />
                <asp:CustomValidator runat="server" CssClass="validator" ID="sizeValidator" ControlToValidate="expensesUpload" ClientValidationFunction="onSizeValidate" ErrorMessage="File must be *.csv and less then 1Mb." meta:resourcekey="sizeValidatorResource1"></asp:CustomValidator>
            </div>

            <asp:GridView ID="expenseGridView" runat="server"
                DataKeyNames="Id"
                AutoGenerateColumns="False"
                CssClass="table table-bordered table-hover"
                BorderWidth="0px"
                EmptyDataText="File doesn't contains valid expenses or expenses from file is already exist." meta:resourcekey="expenseGridViewResource1">
                <Columns>
                    <asp:BoundField HeaderText="Id" DataField="Id" meta:resourcekey="BoundFieldResource1" />
                    <asp:BoundField HeaderText="Details" DataField="Details" meta:resourcekey="BoundFieldResource2" />
                    <asp:BoundField HeaderText="Amount" DataField="Amount" meta:resourcekey="BoundFieldResource3" />
                    <asp:TemplateField HeaderText="Meeting" meta:resourcekey="TemplateFieldResource1">
                        <ItemTemplate>
                            <asp:Label runat="server" ID="meetingnLabel" Text='<%# Eval("Meeting.Name") %>' meta:resourcekey="meetingnLabelResource1"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </div>
        <script type="text/javascript">
            function onSizeValidate(oSrc, args) {
                var fileinput = document.getElementById("expensesUpload");
                var ext = args.Value.substring(args.Value.lastIndexOf('.') + 1).toLowerCase();
                args.IsValid = ext == "csv" && checkFileSize(fileinput, 1);
            }
            function processForm(e) {
                if (e.preventDefault) e.preventDefault();

                return Page_ClientValidate();
            }
        </script>
    </div>
</asp:Content>
