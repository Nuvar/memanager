﻿<%@ Page
    Language="C#"
    MasterPageFile="~/Layouts/ForUsers.master"
    AutoEventWireup="true"
    CodeBehind="Report.aspx.cs"
    Inherits="MeetingManager.ExpensesReport"
    meta:resourcekey="PageResource1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MasterPageForUsers_Content" runat="server">
    <div class="row">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title text-center" runat="server" id="TitleLabel" meta:resourcekey="TitleLabel"></h3>
            </div>

            <div class="panel-body">

                <div class="col-md-4 col-centered">

                    <div class="form-group">
                        <asp:Label runat="server" Text="Start date" AssociatedControlID="StartDateTextBox" meta:resourcekey="LabelResource1" />
                        <asp:TextBox runat="server" ID="StartDateTextBox" CssClass="form-control" meta:resourcekey="StartDateTextBoxResource1"></asp:TextBox>
                        <ajaxToolkit:CalendarExtender runat="server" TargetControlID="StartDateTextBox" ID="ctl011" Enabled="True" />
                    </div>

                    <div class="form-group">
                        <asp:Label runat="server" Text="End date" AssociatedControlID="EndDateTextBox" meta:resourcekey="LabelResource2" />
                        <asp:TextBox runat="server" ID="EndDateTextBox" CssClass="form-control" meta:resourcekey="EndDateTextBoxResource1"></asp:TextBox>
                        <ajaxToolkit:CalendarExtender runat="server" TargetControlID="EndDateTextBox" ID="ctl033" Enabled="True" />
                    </div>

                    <div class="form-group">
                        <asp:RequiredFieldValidator runat="server" ID="StartDateRequiredFieldValidator"
                            ControlToValidate="StartDateTextBox"
                            ValidationGroup="ExpensesReportValidationGroup"
                            ErrorMessage="Start date is required."
                            Display="None" meta:resourcekey="StartDateRequiredFieldValidatorResource1" />
                        <asp:RequiredFieldValidator runat="server" ID="EndDateRequiredFieldValidator"
                            ControlToValidate="EndDateTextBox"
                            ValidationGroup="ExpensesReportValidationGroup"
                            ErrorMessage="End date is required."
                            Display="None" meta:resourcekey="EndDateRequiredFieldValidatorResource1" />
                        <asp:CompareValidator runat="server" ID="DatesCompareValidator"
                            ControlToCompare="StartDateTextBox"
                            ControlToValidate="EndDateTextBox"
                            ValidationGroup="ExpensesReportValidationGroup"
                            Type="Date"
                            Operator="GreaterThanEqual"
                            CultureInvariantValues="True"
                            ErrorMessage="Start date must be earlier than end date"
                            Display="None" meta:resourcekey="DatesCompareValidatorResource1" />
                        <asp:ValidationSummary runat="server" ID="ValidationSummary"
                            ValidationGroup="ExpensesReportValidationGroup" CssClass="alert alert-danger" meta:resourcekey="ValidationSummaryResource1" />
                    </div>

                    <div class="form-group">
                        <asp:Button runat="server" ID="GeneratedButton"
                            ValidationGroup="ExpensesReportValidationGroup"
                            Text="Generate Report"
                            OnClick="GeneratedButton_OnClick"
                            CssClass="btn btn-primary btn-block" meta:resourcekey="GeneratedButtonResource1" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
