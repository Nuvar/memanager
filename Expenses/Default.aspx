﻿<%@ Page Language="C#"
    AutoEventWireup="true"
    CodeBehind="Default.aspx.cs"
    Inherits="MeetingManager.Expenses.Default"
    MasterPageFile="/Layouts/ForUsers.master"
    meta:resourcekey="ExpensesPage" %>

<asp:Content runat="server" ContentPlaceHolderID="MasterPageForUsers_Content">

    <h3 class="text-center" runat="server" id="TitleLabel" meta:resourcekey="TitleLabel"></h3>

    <asp:ObjectDataSource ID="expenseDataSource" runat="server"
        TypeName="MeetingManager.Domain.CRUD.ExpenseRepository"
        DataObjectTypeName="MeetingManager.Domain.Entities.Expense"
        StartRowIndexParameterName="start"
        SelectCountMethod="get_Count"
        MaximumRowsParameterName="max"
        EnablePaging="True"
        SelectMethod="Select"
        DeleteMethod="Delete"
        OnObjectCreating="expenseDataSource_OnObjectCreating" />

    <asp:GridView runat="server" ID="expenseGridView"
        DataKeyNames="Id,Amount,Details"
        DataSourceID="expenseDataSource"
        AutoGenerateColumns="False"
        AllowPaging="True"
        PageSize="20"
        OnRowCommand="ExpenseGridView_RowCommand"
        OnRowDeleting="ExpenseGridView_Deleting"
        CssClass="table table-bordered table-hover td-last-child-200"
        BorderWidth="0px"
        EmptyDataText="No data found" meta:resourcekey="expenseGridViewResource1">
        <Columns>
            <asp:BoundField HeaderText="Id" DataField="Id" meta:resourcekey="IdHeader" />
            <asp:BoundField HeaderText="Amount" DataField="Amount" meta:resourcekey="BoundFieldResource1" />
            <asp:BoundField HeaderText="Details" DataField="Details" meta:resourcekey="BoundFieldResource2" />
            <asp:TemplateField HeaderText="Meeting" meta:resourcekey="TemplateFieldResource1">
                <ItemTemplate>
                    <asp:Label runat="server" ID="meetingLabel" Text='<%# Eval("Meeting.Name") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Command" meta:resourcekey="TemplateFieldResource2">
                <ItemTemplate>
                    <div class="btn-group-vertical btn-group-sm" role="group" aria-label="...">
                        <asp:Button CommandName="Select" CssClass="btn btn-primary" CommandArgument='<%# ((GridViewRow) Container).DataItemIndex %>' Text="<%$Resources:GridViewResource,Select %>" runat="server"></asp:Button>
                        <asp:Button CommandName="Edit" CssClass="btn btn-default" CommandArgument='<%# ((GridViewRow) Container).DataItemIndex %>' Text="<%$Resources:GridViewResource,Edit %>" runat="server"></asp:Button>
                        <asp:Button CommandName="Delete" CssClass="btn btn-danger" CommandArgument='<%# ((GridViewRow) Container).DataItemIndex %>' OnClientClick="if(!confirm(document.getElementById('deleteConfirm').value))return false;" Text="<%$Resources:GridViewResource,Delete %>" runat="server"></asp:Button>
                    </div>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>

    <script type="text/javascript">
        $("tr.pagination > td> table").addClass("table table-bordered table-hover");
        $("tr.pagination > td> table> tbody > tr > td > span").addClass("active");
        $("tr.pagination > td> table> tbody > tr > td > a").addClass("disabled");
    </script>

    <asp:PlaceHolder ID="contentPlace" runat="server" />

</asp:Content>
