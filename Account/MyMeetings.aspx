﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Layouts/ForUsers.master" AutoEventWireup="true" CodeBehind="MyMeetings.aspx.cs" Inherits="MeetingManager.MyMeetings" meta:resourcekey="PageResource1" %>
<%@ Import Namespace="MeetingManager.Domain.Entities" %>
<%@ Import Namespace="Resources" %>

<asp:Content ContentPlaceHolderID="MasterPageForUsers_Content" runat="server">
    <div class="row">
        <h3 class="text-center" runat="server" ID="TitleLabel">My Meetings</h3>
        <div class="row">
            <asp:ObjectDataSource ID="meetingDataSource" runat="server"
                TypeName="MeetingManager.Domain.CRUD.MeetingsRepository"
                DataObjectTypeName="MeetingManager.Domain.Entities.Meeting"
                StartRowIndexParameterName="start"
                SelectCountMethod="SelectCountByIdentity"
                MaximumRowsParameterName="max"
                EnablePaging="True"
                DeleteMethod="Delete"
                SelectMethod="SelectForUser"
                OnObjectCreating="meetingDataSource_ObjectCreating"/>
            <asp:GridView ID="meetingGridView" runat="server"
                DataKeyNames="Id,Name,Description,Date"
                AutoGenerateColumns="False"
                AllowPaging="True"
                PageSize="20"
                OnRowCommand="meetingGridView_RowCommand"
                OnRowDeleting="meetingGridView_RowDeleting"
                CssClass="table table-bordered table-hover td-last-child-200"
                BorderWidth="0px"
                EmptyDataText="Not found meetings for current user" meta:resourcekey="meetingGridViewResource1">
                <Columns>
                    <asp:BoundField HeaderText="Id" DataField="Id" meta:resourcekey="BoundFieldResource1" />
                    <asp:BoundField HeaderText="Name" DataField="Name" meta:resourcekey="BoundFieldResource2" />
                    <asp:BoundField HeaderText="Description" DataField="Description" meta:resourcekey="BoundFieldResource3" />
                    <asp:TemplateField HeaderText="Date" meta:resourcekey="TemplateFieldResource1">
                        <ItemTemplate>
                            <asp:Label runat="server" ID="dateLabel" Text='<%# Convert.ToDateTime(Eval("Date")).ToString("d") %>' meta:resourcekey="dateLabelResource1"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Location" meta:resourcekey="TemplateFieldResource2">
                        <ItemTemplate>
                            <asp:Label runat="server" ID="locationLabel" Text='<%# Eval("Location.Name") %>' meta:resourcekey="locationLabelResource1"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Expenses" meta:resourcekey="TemplateFieldResource3">
                        <ItemTemplate>
                            <asp:Label runat="server" ID="expensesLabel"
                                Text='<%# GridViewResource.Count + ((Meeting)((GridViewRow)Container).DataItem).Expenses.Count + ". "+ GridViewResource.OpenEdit + GetLocalResourceObject("Expenses") %>' meta:resourcekey="expensesLabelResource1"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Participants" meta:resourcekey="TemplateFieldResource4">
                        <ItemTemplate>
                            <asp:Label runat="server" ID="employeeLabel"
                                Text='<%# GridViewResource.Count + ((Meeting)((GridViewRow)Container).DataItem).Employees.Count + ". "+ GridViewResource.OpenEdit + GetLocalResourceObject("Employees") %>' meta:resourcekey="employeeLabelResource1"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Command" meta:resourcekey="TemplateFieldResource5">
                        <ItemTemplate>
                            <div class="btn-group btn-group-sm btn-group-vertical">
                                <asp:Button CommandName="Select" CssClass="btn btn-primary" CommandArgument='<%# ((GridViewRow) Container).DataItemIndex %>' Text="<%$Resources:GridViewResource,Select %>" runat="server"></asp:Button>
                                <asp:Button CommandName="Edit" CssClass="btn btn-default" CommandArgument='<%# ((GridViewRow) Container).DataItemIndex %>' Text="<%$Resources:GridViewResource,Edit %>" runat="server"></asp:Button>
                                <asp:Button CommandName="Delete" CssClass="btn btn-danger" CommandArgument='<%# ((GridViewRow) Container).DataItemIndex %>' OnClientClick="if(!confirm(document.getElementById('deleteConfirm').value))return false;" Text="<%$Resources:GridViewResource,Delete %>" runat="server"></asp:Button>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </div>
        <script type="text/javascript">
            $("tr.pagination > td> table").addClass("table table-bordered table-hover");
            $("tr.pagination > td> table> tbody > tr > td > span").addClass("active");
            $("tr.pagination > td> table> tbody > tr > td > a").addClass("disabled");
        </script>
        <asp:PlaceHolder ID="contentPlace" runat="server"></asp:PlaceHolder>
    </div>
</asp:Content>
