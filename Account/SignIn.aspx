﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SignIn.aspx.cs" Inherits="MeetingManager.Login" MasterPageFile="/Layouts/ForAnonymous.master" meta:resourcekey="PageResource1" %>

<asp:Content ContentPlaceHolderID="MasterPageForAnonymous_Content" runat="server">
    <asp:PlaceHolder ID="contentPlace" runat="server">
        <asp:Login
            DestinationPageUrl="~/Default.aspx"
            DisplayRememberMe="False"
            FailureAction="Refresh"
            FailureText="Login failed. Enter valid email and password."
            ID="loginControl"
            MembershipProvider="SqlCeMembershipProvider"
            Orientation="Vertical"
            PasswordLabelText="Password"
            PasswordRequiredErrorMessage="Please enter your password!"
            runat="server"
            TextLayout="TextOnLeft"
            OnLoginError="Login_Error"
            TitleText="Login"
            UserNameLabelText="Email"
            UserNameRequiredErrorMessage="Please enter your email!"
            VisibleWhenLoggedIn="False"
            RenderOuterTable="False" meta:resourcekey="loginControlResource1">
            <LayoutTemplate>
                <div class="form-signin">
                    <h3 class="form-signin-heading" meta:resourcekey="TitleLable" id="TitleLabel" runat="server"></h3>
                    <asp:CustomValidator runat="server" ID="errorLoginValidator" Display="None" ControlToValidate="UserName" ErrorMessage="Login failed. Enter valid email and password." ValidationGroup="SignInValidationGroup" meta:resourcekey="errorLoginValidatorResource1"></asp:CustomValidator>
                    <div class="form-group">
                        <asp:Label Text="Email" AssociatedControlID="UserName" runat="server"></asp:Label>
                        <asp:TextBox runat="server" type="email" ID="UserName" ClientIDMode="Static" CssClass="form-control" autofocus="autofocus"/>
                        <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1"
                            ControlToValidate="UserName"
                            ValidationGroup="SignInValidationGroup"
                            ErrorMessage="Email is required."
                            ToolTip="Email is required."
                            Display="None" meta:resourcekey="RequiredFieldValidator1Resource1" />
                    </div>
                    <div class="form-group">
                        <asp:Label AssociatedControlID="Password" meta:resourcekey="PasswordLabel" runat="server"></asp:Label>
                        <asp:TextBox runat="server" type="password" ID="Password" ClientIDMode="Static" CssClass="form-control"/>
                        <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator2"
                            ControlToValidate="Password"
                            ValidationGroup="SignInValidationGroup"
                            ErrorMessage="Password is required."
                            ToolTip="Password is required."
                            Display="None" meta:resourcekey="RequiredFieldValidator2Resource1"/>
                    </div>
                    <div class="form-group">
                        <asp:ValidationSummary runat="server" ID="ValidationSummary"
                            ValidationGroup="SignInValidationGroup" CssClass="alert alert-danger"/>
                    </div>
                    <div class="form-group">
                        <asp:Button CssClass="btn btn-lg btn-primary btn-block" ID="Login" CommandName="Login" runat="server" Text="Sign in" ValidationGroup="SignInValidationGroup" meta:resourcekey="LoginResource1" />
                    </div>
                </div>
            </LayoutTemplate>
        </asp:Login>
    </asp:PlaceHolder>
</asp:Content>
