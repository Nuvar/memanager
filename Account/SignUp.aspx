﻿<%@ Page Title="" Language="C#" MasterPageFile="/Layouts/ForAnonymous.master" AutoEventWireup="true" CodeBehind="SignUp.aspx.cs" Inherits="MeetingManager.Registration" meta:resourcekey="SignUpPage"  %>

<asp:Content ID="Content3" ContentPlaceHolderID="MasterPageForAnonymous_Content" runat="server">

    <div class="form-signup">
        <h1><%=GetLocalResourceObject("SignUpPage.Title") %></h1>

        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="Email">Email</asp:Label>
            <asp:TextBox runat="server" ID="Email" CssClass="form-control" />
            <asp:RequiredFieldValidator runat="server" ID="EmailRequiredFieldValidator"
                ControlToValidate="Email"
                ValidationGroup="CreateUserWizardStep1"
                ErrorMessage="Email is required."
                Display="None" meta:resourcekey="EmailRequiredFieldValidatorResource1" />
            <asp:RegularExpressionValidator runat="server" ID="EmailRegularExpressionValidator"
                ControlToValidate="Email"
                ValidationExpression="^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_]+).([a-zA-Z]+)$"
                ValidationGroup="CreateUserWizardStep1"
                ErrorMessage="Email is not valid. Use valid email with ending @nixsolutions.com"
                Display="None" meta:resourcekey="EmailRegularExpressionValidatorResource1" />
        </div>

        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="Password" meta:resourcekey="LabelResource2">Password</asp:Label>
            <asp:TextBox runat="server" ID="Password" CssClass="form-control" TextMode="Password" />
            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator2"
                ControlToValidate="Password"
                ValidationGroup="CreateUserWizardStep1"
                ErrorMessage="Password is required."
                Display="None" meta:resourcekey="RequiredFieldValidator2Resource1" />
            <asp:RegularExpressionValidator runat="server"
                ControlToValidate="Password"
                ValidationGroup="CreateUserWizardStep1"
                ErrorMessage="Password must contain letters and numbers. Length must be from 6 to 16 chars"
                ValidationExpression="\w{6,16}"
                Display="None" meta:resourcekey="RegularExpressionValidatorResource2" />
        </div>

        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="ConfirmPassword" meta:resourcekey="LabelResource3">Confirm password</asp:Label>
            <asp:TextBox runat="server" ID="ConfirmPassword" CssClass="form-control" TextMode="Password"/>
            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator3"
                ControlToValidate="ConfirmPassword"
                ValidationGroup="CreateUserWizardStep1"
                ErrorMessage="Confirm password is required."
                Display="None" meta:resourcekey="RequiredFieldValidator3Resource1" />
        </div>

        <div class="checkbox">
            <asp:Label runat="server" AssociatedControlID="CreateEmployee">
                <asp:CheckBox runat="server" ID="CreateEmployee" CssClass="use-employee" meta:resourcekey="CreateEmployeeResource1" />
            </asp:Label>
            <script type="text/javascript">
                $(document).ready(function () {
                    $(".use-employee").click(function () {
                        var useEmployee = $(this).find("input[type=checkbox]")[0];
                        if (useEmployee === undefined || useEmployee === null) {
                            return;
                        }
                        if (useEmployee.checked) {
                            $(".create-employee").removeClass("hidden");
                        } else {
                            $(".create-employee").addClass("hidden");
                        }
                    });
                });
            </script>
        </div>

        <div runat="server" id="CreateEmployeeContainer" class="create-employee-container">

            <div class="form-group">
                <asp:Label runat="server" AssociatedControlID="EmployeeName" meta:resourcekey="LabelResource5">Name</asp:Label>
                <asp:TextBox runat="server" ID="EmployeeName" CssClass="form-control"  />
                <asp:RequiredFieldValidator runat="server" ID="EmployeeNameRequiredFieldValidator"
                    ControlToValidate="EmployeeName"
                    ValidationGroup="CreateUserWizardStep2"
                    ErrorMessage="Name is required."
                    Display="None" meta:resourcekey="EmployeeNameRequiredFieldValidatorResource1" />
            </div>

            <div class="form-group">
                <asp:Label runat="server" AssociatedControlID="EmployeeDepartment" meta:resourcekey="LabelResource6">Department</asp:Label>
                <asp:TextBox runat="server" ID="EmployeeDepartment" CssClass="form-control"/>
            </div>

            <div class="form-group">
                <asp:Label runat="server" AssociatedControlID="EmployeePhone" meta:resourcekey="LabelResource7">Phone</asp:Label>
                <div class="input-group">
                    <div class="input-group-addon">+380</div>
                    <asp:TextBox runat="server" ID="EmployeePhone" CssClass="form-control" />
                </div>
                <asp:RegularExpressionValidator runat="server" ID="EmployeePhoneRegularExpressionValidator"
                    ControlToValidate="EmployeePhone"
                    ValidationExpression="^(?=\d{2}\d{5,7}).*$"
                    ValidationGroup="CreateUserWizardStep2"
                    ErrorMessage="Phone is not valid Ukrainian phone number. Phone must should not contain any symbols, other than numbers. +380XXXXXXX"
                    Display="None" meta:resourcekey="EmployeePhoneRegularExpressionValidatorResource1" />
            </div>

            <div class="form-group">
                <asp:Label runat="server" AssociatedControlID="EmployeeBirthday" meta:resourcekey="LabelResource8">Birthday</asp:Label>
                <asp:TextBox runat="server" ID="EmployeeBirthday" CssClass="form-control"/>
                <ajaxToolkit:CalendarExtender runat="server" ID="CalendarExtender"
                    TargetControlID="EmployeeBirthday" Enabled="True"  />
                <asp:CustomValidator runat="server" ID="EmployeeBirthdayCustomValidator"
                    ControlToValidate="EmployeeBirthday"
                    ValidationGroup="CreateUserWizardStep2"
                    ErrorMessage="Birthday is not valid."
                    Display="None"
                    OnServerValidate="EmployeeBirthdayCustomValidator_OnServerValidate" meta:resourcekey="EmployeeBirthdayCustomValidatorResource1" />
            </div>

        </div>

        <div class="form-group">

            <asp:CompareValidator runat="server" ID="PasswordCompare"
                ControlToCompare="Password"
                ControlToValidate="ConfirmPassword"
                ValidationGroup="CreateUserWizardStep1"
                ErrorMessage="The password and confirmation password must match."
                Display="None" meta:resourcekey="PasswordCompareResource1" />
            <asp:Panel runat="server" ID="ErrorMessageContainer" ClientIDMode="Static" CssClass="alert alert-danger hidden">
                <asp:ValidationSummary runat="server" ID="ValidationSummary"
                    ValidationGroup="CreateUserWizardStep1"/>
                <asp:ValidationSummary runat="server" ID="ValidationSummary1"
                    ValidationGroup="CreateUserWizardStep2"/>
                <div>
                    <asp:PlaceHolder runat="server" ID="ErrorMessagePlaceHolder" />
                </div>
            </asp:Panel>
        </div>


        <div class="form-group">
            <asp:Button runat="server" ID="SignUpButton"
                ValidationGroup="CreateUserWizardStep1"
                CommandName="MoveNext"
                CssClass="btn btn-lg btn-success btn-block"
                Text="Sign up"
                OnClick="SignUpButton_OnClick" OnClientClick="return CreateUserValidation();" meta:resourcekey="SignUpButtonResource1" />
            <script type="text/javascript">
                function CreateUserValidation() {
                    var useEmployee = $(".use-employee > input[type=checkbox]")[0];
                    var isValid;
                    if (useEmployee === undefined || useEmployee === null) {
                        isValid = Page_ClientValidate("CreateUserWizardStep1");
                    } else {
                        isValid = Page_ClientValidate("CreateUserWizardStep1");
                        if (isValid) {
                            isValid = (useEmployee.checked ? Page_ClientValidate("CreateUserWizardStep2") : true);
                        }
                    }
                    if (!isValid) {
                        $("#ErrorMessageContainer").removeClass("hidden", 0.5);
                    } else {
                        $("#ErrorMessageContainer").addClass("hidden", 0.5);
                    }
                    return isValid;
                }
            </script>
        </div>
    </div>
</asp:Content>
