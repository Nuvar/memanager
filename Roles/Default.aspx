﻿<%@ Page
    Language="C#"
    AutoEventWireup="true"
    CodeBehind="Default.aspx.cs"
    Inherits="MeetingManager.Roles.Default"
    MasterPageFile="/Layouts/ForUsers.master"
    meta:resourcekey="PageResource1" %>

<asp:Content runat="server" ContentPlaceHolderID="MasterPageForUsers_Content">

    <asp:HiddenField runat="server" ID="roleWarning" ClientIDMode="Static" meta:resourcekey="roleWarning" />

    <h3 class="text-center" runat="server" id="TitleLabel" meta:resourcekey="TitleLabel"></h3>

    <asp:GridView runat="server" ID="rolesGridView"
        DataKeyNames="Name"
        ItemType="MeetingManager.Domain.Entities.Role"
        AutoGenerateColumns="False"
        AllowPaging="True"
        PageSize="20"
        OnRowEditing="Role_Editing"
        OnRowDeleting="Role_Deleting"
        OnRowDeleted="Role_Deleted"
        OnRowCancelingEdit="Role_CancelEdit"
        OnRowUpdating="Role_Updating"
        ShowHeaderWhenEmpty="True"
        OnRowCommand="Role_Inserting"
        OnPageIndexChanging="GridView_PageChanged"
        CssClass="table table-bordered table-hover td-last-child-200"
        BorderWidth="0px"
        EmptyDataText="There are no roles in system" meta:resourcekey="rolesGridViewResource1">
        <Columns>
            <asp:TemplateField HeaderText="Role Name" meta:resourcekey="TemplateFieldResource1">
                <ItemTemplate>
                    <asp:Label runat="server" Text='<%# Eval("Name") == null ? null : Eval("Name").ToString() %>'></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox runat="server" ID="editRole" CssClass="form-control" ValidationGroup="EditRoleGroup" Text='<%# Eval("Name").ToString() %>'></asp:TextBox>
                    <asp:RequiredFieldValidator runat="server" ControlToValidate="editRole" CssClass="validator" ValidationGroup="EditRoleGroup" Display="None" ErrorMessage="Role name is required" meta:resourcekey="RequiredFieldValidatorResource1"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator runat="server" ControlToValidate="editRole" CssClass="validator" ValidationExpression="\w+" Display="None" ValidationGroup="EditRoleGroup" ErrorMessage="Role name must contains from 3 to 16 characters. Characters are numbers and letters" meta:resourcekey="RegularExpressionValidatorResource1"></asp:RegularExpressionValidator>
                    <asp:CustomValidator runat="server" ControlToValidate="editRole" CssClass="validator" ValidationGroup="EditRoleGroup" Display="None" ErrorMessage="Role with such name is already exist" OnServerValidate="RoleServerValidating" meta:resourcekey="CustomValidatorResource1"></asp:CustomValidator>
                    <asp:ValidationSummary runat="server" CssClass="alert alert-danger" ValidationGroup="EditRoleGroup" />
                </EditItemTemplate>
                <HeaderTemplate>
                    <asp:TextBox runat="server" ID="insertRole" CssClass="form-control" ValidationGroup="InsertRoleGroup"></asp:TextBox>
                    <asp:RequiredFieldValidator runat="server" ControlToValidate="insertRole" CssClass="validator" ValidationGroup="InsertRoleGroup" Display="None" ErrorMessage="Role name is required" meta:resourcekey="RequiredFieldValidatorResource2"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator runat="server" ControlToValidate="insertRole" CssClass="validator" ValidationExpression="\w+" Display="None" ValidationGroup="InsertRoleGroup" ErrorMessage="Role name must contains from 3 to 16 characters. Characters are numbers and letters" meta:resourcekey="RegularExpressionValidatorResource2"></asp:RegularExpressionValidator>
                    <asp:CustomValidator runat="server" ControlToValidate="insertRole" CssClass="validator" ValidationGroup="InsertRoleGroup" Display="None" ErrorMessage="Role with such name is already exist" OnServerValidate="RoleServerValidating" meta:resourcekey="CustomValidatorResource2"></asp:CustomValidator>
                    <asp:ValidationSummary runat="server" CssClass="alert alert-danger" ValidationGroup="InsertRoleGroup" />
                </HeaderTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Actions" meta:resourcekey="TemplateFieldResource2">
                <ItemTemplate>
                    <div class="btn-group-vertical btn-group-sm" role="group" aria-label="...">
                        <asp:Button CommandName="Edit" Text="Edit" runat="server" CssClass="btn btn-default" meta:resourcekey="ButtonResource4" />
                        <asp:Button runat="server" Text="Delete" CssClass="btn btn-danger" OnClientClick="if(!confirm(document.getElementById('deleteConfirm').value + document.getElementById('roleWarning').value))return false;" CommandName="Delete" meta:resourcekey="ButtonResource5" />
                    </div>
                </ItemTemplate>
                <EditItemTemplate>
                    <div class="btn-group-vertical btn-group-sm" role="group" aria-label="...">
                        <asp:Button CommandName="Update" ValidationGroup="EditRoleGroup" Text="Update" runat="server" CssClass="btn btn-success" meta:resourcekey="ButtonResource1" />
                        <asp:Button runat="server" Text="Cancel" CssClass="btn btn-default" CommandName="Cancel" meta:resourcekey="ButtonResource2" />
                    </div>
                </EditItemTemplate>
                <HeaderTemplate>
                    <div class="btn-group-vertical btn-group-sm" role="group" aria-label="...">
                        <asp:Button runat="server" CommandName="Insert" CssClass="btn btn-primary" Text="Add role" ValidationGroup="InsertRoleGroup" meta:resourcekey="ButtonResource3" />
                    </div>
                </HeaderTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>

    <script type="text/javascript">
        $("tr.pagination > td> table").addClass("table table-bordered table-hover");
        $("tr.pagination > td> table> tbody > tr > td > span").addClass("active");
        $("tr.pagination > td> table> tbody > tr > td > a").addClass("disabled");
    </script>

</asp:Content>


