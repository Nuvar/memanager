﻿__employeeSearchPageSize = 10;
__employeeSearchCurrentPage = 0;

$().ready(function () {
    $("#btnSearch").click(function () {
        if (tbName.value.length > 0 || tbDepartment.value.length > 0) {
            sendRequest(createJson());
        }
        else {
            lblInfo.textContent = $(validationErrorMessage_NoParams).val();
        }
    });
});

function createJson() {
    var obj = {
        "name": tbName.value,
        "department": tbDepartment.value,
        "searchMode": getSearchMode(),
        "pageNumber": __employeeSearchCurrentPage,
        "pageSize": __employeeSearchPageSize
    };
    return obj;
}

function sendRequest(parameters) {
    $.ajax({
        type: "POST",
        url: $(ExternalServicePath).val(),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: JSON.stringify(parameters),
        beforeSend: function () {
            $("#ajaxLoader").show();
        },
        success: function (data) {
            clearTable();
            var response = JSON.parse(data.d);
            if (response.Employees.length > 0) {
                response["ActionLinks"] = setActionLinks(response);
                showResponseData(response);
                __searchResultId = response.ResultId;
                $("#lblInfo").hide();
                $("#content").show();
                showPagination(response.TotalCount);
            }
            else {
                $(lblInfo).text($(resultMessage_NoFound).val()).show();
                $("#content").hide();
                $("#pagination").empty();
            }
            $("#ajaxLoader").hide();
        },
        error: function () {
            $("#ajaxLoader").hide();
            console.error("Error employees search functuon - sendRequest");
        }
    });
}

function getSearchMode() {
    var selectedValue = selectSearchMode.value;
    if (selectedValue == "")
        return "BeginWith";
    else
        return selectedValue;
}

function setActionLinks(data) {
    var employees = data.Employees,
        employeesIds = [],
        result = null;

    for (var i = 0; i < employees.length; i++) {
        employeesIds[i] = employees[i].EmployeeId;
    }

    $.ajax({
        type: "POST",
        url: "../Services/EmployeesService.asmx/GetActionLinks",
        async: false,
        contentType: "application/json",
        dataType: "json",
        data: JSON.stringify({ ids: employeesIds })
    }).done(function (data) {
        result = data.d;
    });

    return result;
}

function showResponseData(data) {
    var employees = data.Employees,
        links = data.ActionLinks;

    for (var i = 0; i < employees.length; i++) {
        createTableRow({
            'Id': employees[i].EmployeeId,
            'Name': employees[i].Name,
            'Department': employees[i].Department,
            'Phone': employees[i].Phone,
            'Birthday': new Date(employees[i].Birthday).toLocaleDateString(),
            'ActionLink': links[i]
        });
    }
}

function clearTable() {
    var rows = $("#dataTable").children().first().children();
    for (var i = 1; i < rows.length; i++) {
        $(rows[i]).remove();
    }
}

function createTableRow(data) {
    var row = $("<tr>"),
        name = $("<td>").text(data.Name),
        department = $("<td>").text(data.Department),
        phone = $("<td>").text(data.Phone),
        birthday = $("<td>").text(data.Birthday),
        action = $("<td>");

    if (data.ActionLink == null) {
        createButton($(btnAdd).val(), $(btnAddTooltip).val())
            .data("id", data.Id)
            .click(function () {
                var el = this;
                $.ajax({
                    type: "POST",
                    url: "../Services/EmployeesService.asmx/AddEmployee",
                    async: false,
                    contentType: "application/json",
                    dataType: "json",
                    data: JSON.stringify({
                        id: data.Id,
                        name: data.Name,
                        department: data.Department,
                        phone: data.Phone,
                        birthday: data.Birthday
                    })
                }).done(function (data) {
                    $(el).replaceWith(createLinkButton($(btnView).val(), data.d, $(btnViewTooltip).val()));
                });
            })
            .appendTo(action);
    } else {
        createLinkButton($(btnView).val(), data.ActionLink, $(btnViewTooltip).val()).appendTo(action);
    }

    row.append(name)
        .append(department)
        .append(phone)
        .append(birthday)
        .append(action);

    $("#dataTable").children().first().append(row);
}

function createButton(text, tooltip) {
    return $("<button>").text(text)
        .attr("type", "button")
        .attr("title", tooltip)
        .addClass("btn btn-link");
}

function createLinkButton(text, href, tooltip) {
    return $("<a>").text(text)
        .attr("title", tooltip)
        .attr("href", href + "&redirect=Search.aspx")
        .addClass("btn btn-link");
}

function createHeaderTableRow(data) {
    var row = document.createElement("tr");
    for (var i = 0; i < data.length; i++) {
        var cell = document.createElement("th");
        cell.innerHTML = data[i];
        row.appendChild(cell);
    }
    $("#dataTable").children().first().append(row);
}

function showPagination(totalCount) {
    $("#pagination").empty();
    var pages = getPagesCount(totalCount);
    for (var i = 0; i < pages; i++) {
        var link = $("<a>").data("id", i).text(i + 1).click(function () {
            __employeeSearchCurrentPage = $(this).data("id");
            sendRequest(createJson());
            return false;
        });
        $("<li>").append(link).appendTo(pagination);
    }
    setCurrentPage();
}

function getPagesToDisplay() {
    var startPage = __employeeSearchCurrentPage - 2;
    var endPage = __employeeSearchCurrentPage + 2;
    if (startPage < 1) {
        startPage = 1;
        endPage = 5;
    }
    var pagesCount = getPagesCount();
    if (endPage > pagesCount) {
        startPage = pagesCount - 5;
        endPage = pagesCount;
    }
    var pageNumbers = new Array();
    for (var i = startPage, count = 0; i < endPage + 1; i++, count++) {
        pageNumbers[count] = i;
    }
    return pageNumbers;
}

function getCurrentPage() {
    return __employeeSearchCurrentPage;
}

function setCurrentPage() {
    $("#pagination").find(".active").first().removeClass("active");
    var navLinks = $("#pagination").children();
    for (var i = 0; i < navLinks.length; i++) {
        if ($(navLinks[i]).children().first().text() == __employeeSearchCurrentPage + 1) {
            $(navLinks[i]).addClass('active');
            __employeeSearchCurrentPage = i;
            break;
        }
    }
}

function getPagesCount(totalCount) {
    return Math.ceil(totalCount / __employeeSearchPageSize);
}