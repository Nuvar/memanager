﻿function checkFileSize(fileinput, maxSize) {
    try {
        var fileSize = 0;
        fileSize = fileinput.files[0].size;
        fileSize = fileSize / 1048576;
        return fileSize <= maxSize;
    }
    catch (e) {
        alert("Error is :" + e);
        return true;
    }
}

function checkIE() {
    var ua = window.navigator.userAgent;
    var msie = ua.indexOf("MSIE ");

    return msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./);
}