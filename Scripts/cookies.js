﻿function setLanguageCookie() {
    var language;
    var date = new Date();
    date.setYear(date.getYear() + 5);
    if (document.cookie == "language=en") language = "ru";
    else if (document.cookie == "language=ru") language = "en";
    document.cookie = "language=" + language + "; path=/; expires=Thu, 01 Jan 2020 00:00:00 UTC";
}