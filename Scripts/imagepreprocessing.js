﻿function redrawImage(event, id) {
    var target = event.target ? event.target : event.srcElement;
    var file = target.files[0];
    var output = document.getElementById(id);
    output.src = URL.createObjectURL(file);
}

function resetImage(fileUploadId, pictureId) {
    var oldInput = document.getElementById(fileUploadId);

    var newInput = document.createElement("input");

    newInput.type = "file";
    newInput.id = oldInput.id;
    newInput.name = oldInput.name;
    newInput.className = oldInput.className;
    newInput.style.cssText = oldInput.style.cssText;
    newInput.onchange = function () {
        var ev = {};
        ev.target = newInput;
        redrawImage(ev, pictureId);
    };
    oldInput.parentNode.replaceChild(newInput, oldInput);
    document.getElementById(pictureId).src = "Handlers/LoadImage.ashx?Id=-1";

    return false;
}

function flushImages() {
    var choser = $("#imageChooser");
    choser.replaceWith(choser = choser.clone(true));
    document.getElementById("imageBox").src = "Handlers/LoadImage.ashx?Id=-1";
}
