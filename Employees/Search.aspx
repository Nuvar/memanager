﻿<%@ Page
    Language="C#"
    MasterPageFile="/Layouts/ForUsers.master"
    AutoEventWireup="true"
    CodeBehind="Search.aspx.cs"
    Inherits="MeetingManager.Employees.Search"
    meta:resourcekey="Search" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MasterPageForUsers_Content" runat="server">
    <div class="row">

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="text-center" runat="server" id="TitleLabel" meta:resourcekey="TitleLabel"></h3>
            </div>

            <div class="panel-body">
                <div class="col-md-10 col-centered">
                    <div>
                        <div class="form-group">
                            <asp:Label runat="server" meta:resourcekey="lblName" AssociatedControlID="tbName"></asp:Label>
                            <asp:TextBox runat="server" CssClass="form-control" ID="tbName" ClientIDMode="Static"></asp:TextBox>
                        </div>

                        <div class="form-group">
                            <asp:Label runat="server" AssociatedControlID="tbDepartment" meta:resourcekey="lblDepartment"></asp:Label>
                            <asp:TextBox runat="server" CssClass="form-control" ID="tbDepartment" ClientIDMode="Static"></asp:TextBox>
                        </div>

                        <div class="form-group">
                            <asp:Label runat="server" AssociatedControlID="selectSearchMode" meta:resourcekey="lblSearchMode"></asp:Label>
                            <asp:DropDownList runat="server" CssClass="form-control" ID="selectSearchMode" ClientIDMode="Static">
                                <asp:ListItem Value="" Text="Search mode" meta:resourcekey="liSearchMode"></asp:ListItem>
                                <asp:ListItem Value="BeginWith" meta:resourcekey="liBeginWidth"></asp:ListItem>
                                <asp:ListItem Value="Contains" meta:resourcekey="liContains"></asp:ListItem>
                                <asp:ListItem Value="Equals" meta:resourcekey="liEquals"></asp:ListItem>
                            </asp:DropDownList>
                        </div>

                        <asp:Button runat="server" CssClass="btn btn-primary" ID="btnSearch" OnClientClick="return false;" meta:resourcekey="btnSearch" ClientIDMode="Static" />

                        <asp:PlaceHolder runat="server" ID="HiddenFieldsPlaceHolder" />

                    </div>

                    <asp:Panel runat="server" ID="content" CssClass="table-responsive" ClientIDMode="Static" />

                    <div style="margin-top: 50px;">
                        <label id="lblInfo"></label>
                    </div>
                    <div id="ajaxLoader" style="margin-left: 50%; display: none;">
                        <img src="/Content/Images/ajax-loader.gif" />
                    </div>
                    <nav>
                        <ul class="pagination" id="pagination">
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
