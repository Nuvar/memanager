﻿<%@ Page
    Language="C#"
    MasterPageFile="/Layouts/ForUsers.master"
    AutoEventWireup="true"
    CodeBehind="Default.aspx.cs"
    Inherits="MeetingManager.Employees.Default"
    meta:resourcekey="EmployeePage" %>

<%@ Import Namespace="MeetingManager.Domain.Extensions" %>
<%@ Import Namespace="MeetingManager.Domain.Entities" %>
<%@ Import Namespace="Resources" %>

<asp:Content ContentPlaceHolderID="MasterPageForUsers_Content" runat="server">

    <h3 class="text-center"><%=GetLocalResourceObject("EmployeePage.Title") %></h3>

    <asp:ObjectDataSource ID="employeeDataSource" runat="server"
        TypeName="MeetingManager.Domain.CRUD.EmployeeRepository"
        DataObjectTypeName="MeetingManager.Domain.Entities.Employee"
        StartRowIndexParameterName="start"
        SelectCountMethod="get_Count"
        MaximumRowsParameterName="max"
        EnablePaging="True"
        SelectMethod="Select"
        DeleteMethod="Delete"
        OnObjectCreating="employeeDataSource_OnObjectCreating" />

    <asp:GridView ID="employeeGridView" runat="server"
        DataKeyNames="Id,Name,Department,Phone,Birthday,Meetings"
        DataSourceID="employeeDataSource"
        AutoGenerateColumns="False"
        AllowPaging="True"
        PageSize="20"
        OnRowCommand="EmployeeGridView_RowCommand"
        OnRowDeleting="EmployeeGridView_RowDeleting"
        CssClass="table table-bordered table-hover td-last-child-200"
        BorderWidth="0px"
        EmptyDataText="No data found" meta:resourcekey="EmployeeGrid">
        <Columns>
            <asp:BoundField HeaderText="Id" DataField="Id" meta:resourcekey="IdField" />
            <asp:BoundField HeaderText="Name" DataField="Name" meta:resourcekey="NameField" />
            <asp:BoundField HeaderText="Department" DataField="Department" meta:resourcekey="DepartmentField" />
            <asp:TemplateField HeaderText="Phone" meta:resourcekey="PhoneField">
                <ItemTemplate>
                    <asp:Label runat="server" Text='<%# String.IsNullOrEmpty(Eval("Phone") as string) ? "" : "+380" + Eval("Phone") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Birthday" meta:resourcekey="BirthdayField">
                <ItemTemplate>
                    <asp:Label runat="server" Text='<%# Convert.ToDateTime(Eval("Birthday")).ToString("d") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="User" meta:resourcekey="UserField">
                <ItemTemplate>
                    <asp:Label runat="server" Text='<%# ((Employee)((GridViewRow)Container).DataItem).GetUser() == null ? String.Empty :
                            ((Employee)((GridViewRow)Container).DataItem).GetUser().UserName %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Meetings" meta:resourcekey="MeetingsField">
                <ItemTemplate>
                    <asp:Label runat="server" Text='<%# GridViewResource.Count + (Eval("Meetings.Count") ?? "0") + ". "+ GridViewResource.OpenEdit + GetLocalResourceObject("Meetings") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Command" meta:resourcekey="CommandField">
                <ItemTemplate>
                    <div class="btn-group-vertical btn-group-sm" role="group" aria-label="...">
                        <asp:Button CommandName="Select" CssClass="btn btn-primary" CommandArgument='<%# ((GridViewRow) Container).DataItemIndex %>' Text="<%$Resources:GridViewResource,Select %>" runat="server"></asp:Button>
                        <asp:Button CommandName="Edit" CssClass="btn btn-default" CommandArgument='<%# ((GridViewRow) Container).DataItemIndex %>' Text="<%$Resources:GridViewResource,Edit %>" runat="server"></asp:Button>
                        <asp:Button CommandName="Delete" CssClass="btn btn-danger" CommandArgument='<%# ((GridViewRow) Container).DataItemIndex %>' OnClientClick="if(!confirm(document.getElementById('deleteConfirm').value))return false;" Text="<%$Resources:GridViewResource,Delete %>" runat="server"></asp:Button>
                    </div>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>

    <script type="text/javascript">
        $("tr.pagination > td> table").addClass("table table-bordered table-hover");
        $("tr.pagination > td> table> tbody > tr > td > span").addClass("active");
        $("tr.pagination > td> table> tbody > tr > td > a").addClass("disabled");
    </script>

    <asp:PlaceHolder ID="contentPlace" runat="server" />

</asp:Content>
