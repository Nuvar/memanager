﻿<%@ Page
    Language="C#"
    MasterPageFile="/Layouts/ForUsers.master"
    AutoEventWireup="true"
    CodeBehind="Default.aspx.cs"
    Inherits="MeetingManager.NavigationLinks.Default"
    meta:resourcekey="PageResource1" %>

<%@ Import Namespace="MeetingManager.Domain.Extensions" %>
<%@ Import Namespace="MeetingManager.Domain.Entities" %>

<asp:Content ID="Content" ContentPlaceHolderID="MasterPageForUsers_Content" runat="server">

    <h3 class="text-center" runat="server" id="TitleLabel" meta:resourcekey="TitleLabel"></h3>

    <asp:ObjectDataSource ID="navLinkDataSource" runat="server"
        TypeName="MeetingManager.Domain.CRUD.NavigationLinksRepository"
        DataObjectTypeName="MeetingManager.Domain.Entities.NavigationLink"
        StartRowIndexParameterName="start"
        SelectCountMethod="get_Count"
        MaximumRowsParameterName="max"
        EnablePaging="True"
        SelectMethod="Select"
        DeleteMethod="Delete" OnObjectCreating="navLinkDataSource_OnObjectCreating" />

    <asp:GridView ID="navLinkGridView" runat="server"
        DataKeyNames="Id,Url,Text,IconId,Roles"
        DataSourceID="navLinkDataSource"
        AutoGenerateColumns="False"
        AllowPaging="True"
        OnRowCommand="NavLinkGridView_RowCommand"
        OnRowDeleting="NavLinkGridView_RowDeleting"
        CssClass="table table-bordered table-hover td-last-child-200"
        BorderWidth="0px"
        EmptyDataText="No data found"
        meta:resourcekey="navLinkGridViewResource1">
        <Columns>
            <asp:BoundField HeaderText="Id" DataField="Id" meta:resourcekey="BoundFieldResource1" />
            <asp:TemplateField HeaderText="Url" meta:resourcekey="TemplateFieldResource1">
                <ItemTemplate>
                    <a href='<%# Eval("Url") %>'><%# Server.HtmlEncode(Eval("Url").ToString()) %></a>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField HeaderText="Text" DataField="Text" meta:resourcekey="BoundFieldResource2" />
            <asp:TemplateField HeaderText="Image" meta:resourcekey="TemplateFieldResource2">
                <ItemTemplate>
                    <asp:Image ImageUrl='<%# "~/Handlers/LoadImage.ashx?Id=" + (Eval("IconId") ?? "-1") %>' runat="server" CssClass="grid-image-icon" meta:resourcekey="ImageResource1" />
                    <asp:Image ImageUrl='<%# "~/Handlers/LoadImage.ashx?Id=" + (Eval("IconId") ?? "-1") %>' runat="server" CssClass="grid-image-view" meta:resourcekey="ImageResource2" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Visible for roles" meta:resourcekey="TemplateFieldResource3">
                <ItemTemplate>
                    <asp:Label runat="server" Text='<%# ((NavigationLink)((GridViewRow)Container).DataItem).GetRoles() %>' meta:resourcekey="LabelResource1"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Command" meta:resourcekey="TemplateFieldResource4">
                <ItemTemplate>
                    <div class="btn-group-vertical btn-group-sm" role="group" aria-label="...">
                        <asp:Button CommandName="Select" CssClass="btn btn-primary" CommandArgument='<%# ((GridViewRow) Container).DataItemIndex %>' Text="<%$Resources:GridViewResource,Select %>" runat="server"></asp:Button>
                        <asp:Button CommandName="Edit" CssClass="btn btn-default" CommandArgument='<%# ((GridViewRow) Container).DataItemIndex %>' Text="<%$Resources:GridViewResource,Edit %>" runat="server"></asp:Button>
                        <asp:Button CommandName="Delete" CssClass="btn btn-danger" CommandArgument='<%# ((GridViewRow) Container).DataItemIndex %>' OnClientClick="if(!confirm(document.getElementById('deleteConfirm').value))return false;" Text="<%$Resources:GridViewResource,Delete %>" runat="server"></asp:Button>
                    </div>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>

    <script type="text/javascript">
        $("tr.pagination > td> table").addClass("table table-bordered table-hover");
        $("tr.pagination > td> table> tbody > tr > td > span").addClass("active");
        $("tr.pagination > td> table> tbody > tr > td > a").addClass("disabled");
    </script>

    <asp:PlaceHolder runat="server" ID="linkManagerContent" />

</asp:Content>
