﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="MeetingManager.Locations.Default" MasterPageFile="/Layouts/ForUsers.master" meta:resourcekey="PageResource1" %>

<asp:Content runat="server" ContentPlaceHolderID="MasterPageForUsers_Content">

    <h3 class="text-center" runat="server" id="TitleLabel" meta:resourcekey="TitleLabel"></h3>

    <asp:ObjectDataSource runat="server" ID="LocationsDataSource"
        TypeName="MeetingManager.Domain.CRUD.LocationsRepository"
        DataObjectTypeName="MeetingManager.Domain.Entities.Location"
        StartRowIndexParameterName="start"
        MaximumRowsParameterName="max"
        SelectCountMethod="get_Count"
        EnablePaging="True"
        SelectMethod="Select"
        DeleteMethod="Delete"
        OnObjectCreating="LocationsDataSource_OnObjectCreating" />

    <asp:GridView runat="server" ID="LocationsGridView"
        DataKeyNames="Id,Name,Address"
        DataSourceID="LocationsDataSource"
        AutoGenerateColumns="False"
        AllowPaging="True"
        PageSize="20"
        OnSelectedIndexChanging="_locationsGridView_SelectedIndexChanging"
        OnRowEditing="_locationsGridView_RowEditing"
        OnRowDeleted="_locationsGridView_RowDeleted"
        CssClass="table table-bordered table-hover td-last-child-200"
        BorderWidth="0px"
        EmptyDataText="No data found" meta:resourcekey="LocationsGridViewResource1">
        <Columns>
            <asp:BoundField HeaderText="Id" DataField="Id" meta:resourcekey="BoundFieldResource1" />
            <asp:BoundField HeaderText="Name" DataField="Name" meta:resourcekey="BoundFieldResource2" />
            <asp:BoundField HeaderText="Address" DataField="Address" meta:resourcekey="BoundFieldResource3" />
            <asp:TemplateField HeaderText="Command" meta:resourcekey="TemplateFieldResource1">
                <ItemTemplate>
                    <div class="btn-group-vertical btn-group-sm" role="group" aria-label="...">
                        <asp:Button CommandName="Select" CssClass="btn btn-primary" CommandArgument='<%# ((GridViewRow) Container).DataItemIndex %>' Text="<%$Resources:GridViewResource,Select %>" runat="server"></asp:Button>
                        <asp:Button CommandName="Edit" CssClass="btn btn-default" CommandArgument='<%# ((GridViewRow) Container).DataItemIndex %>' Text="<%$Resources:GridViewResource,Edit %>" runat="server"></asp:Button>
                        <asp:Button CommandName="Delete" CssClass="btn btn-danger" CommandArgument='<%# ((GridViewRow) Container).DataItemIndex %>' OnClientClick="if(!confirm(document.getElementById('deleteConfirm').value))return false;" Text="<%$Resources:GridViewResource,Delete %>" runat="server"></asp:Button>
                    </div>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>

    <script type="text/javascript">
        $("tr.pagination > td> table").addClass("table table-bordered table-hover");
        $("tr.pagination > td> table> tbody > tr > td > span").addClass("active");
        $("tr.pagination > td> table> tbody > tr > td > a").addClass("disabled");
    </script>

    <asp:PlaceHolder runat="server" ID="LocationContent" />

</asp:Content>
