﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Layouts/ForAnonymous.master" AutoEventWireup="true" CodeBehind="UnauthorizedAccess.aspx.cs" Inherits="MeetingManager.UnauthorizedAccess" meta:resourcekey="PageResource1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MasterPageForAnonymous_Content" runat="server">
    <h2><%=GetLocalResourceObject("PageResource1.Title") %></h2>
    <p>
        <%=GetLocalResourceObject("ErrorMessage") %>
    </p>
    <p>
        <%=GetLocalResourceObject("Recommendation") %>
    </p>
</asp:Content>
