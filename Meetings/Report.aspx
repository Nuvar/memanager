﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Layouts/ForUsers.master" AutoEventWireup="true" CodeBehind="Report.aspx.cs" Inherits="MeetingManager.Meetings.Report" meta:resourcekey="PageResource1" %>

<asp:Content ContentPlaceHolderID="MasterPageForUsers_Content" runat="server">
    <div class="row">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="text-center" runat="server" ID="TitleLabel" meta:resourcekey="TitleLabel"></h3>
            </div>
            <div class="panel-body">
                <div class="col-md-4 col-centered">
                    <div class="form-group">
                        <asp:Label runat="server" Text="Select location" AssociatedControlID="locationsDropDown" meta:resourcekey="LabelResource1" />
                        <asp:DropDownList runat="server" CssClass="form-control" ID="locationsDropDown" meta:resourcekey="locationsDropDownResource1" />
                    </div>
                    <div class="form-group">
                        <asp:Button runat="server"
                            Text="Generate Report"
                            CssClass="btn btn-primary btn-block"
                            OnClick="Generate_Click" meta:resourcekey="ButtonResource1" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>