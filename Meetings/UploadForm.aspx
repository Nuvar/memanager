﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UploadForm.aspx.cs" Inherits="MeetingManager.Meetings.UploadForm" MasterPageFile="/Layouts/ForUsers.master" meta:resourcekey="PageResource1" %>
<asp:Content runat="server" ContentPlaceHolderID="MasterPageForUsers_Content" ClientIDMode="Static">
    <div class="row">
        <h3 class="text-center" runat="server" ID="TitleLabel" meta:resourcekey="TitleLabel"></h3>
        <div class="form-group">
            <h4 class="text-center" runat="server" ID="SelectLabel" meta:resourcekey="SelectLabel"></h4>
            <asp:FileUpload runat="server" ID="meetingsUpload" ClientIDMode="Static" data-show-preview="false" CssClass="file form-control" meta:resourcekey="meetingsUploadResource1" />
            <asp:CustomValidator runat="server" CssClass="validator" ID="sizeValidator" ControlToValidate="meetingsUpload" ClientValidationFunction="onSizeValidate" ErrorMessage="File must be *.csv and less then 1Mb." meta:resourcekey="sizeValidatorResource1"></asp:CustomValidator>
            <asp:GridView ID="meetingGridView" runat="server"
                DataKeyNames="Id,Name"
                AutoGenerateColumns="False"
                CssClass="table table-bordered table-hover"
                BorderWidth="0px"
                EmptyDataText="File doesn't contains valid meetings or meetings from file is already exist." meta:resourcekey="meetingGridViewResource1">
                <Columns>
                    <asp:BoundField HeaderText="Id" DataField="Id" meta:resourcekey="BoundFieldResource1" />
                    <asp:BoundField HeaderText="Name" DataField="Name" meta:resourcekey="BoundFieldResource2" />
                    <asp:BoundField HeaderText="Description" DataField="Description" meta:resourcekey="BoundFieldResource3" />
                    <asp:TemplateField HeaderText="Date" meta:resourcekey="TemplateFieldResource1">
                        <ItemTemplate>
                            <asp:Label runat="server" ID="dateLabel" Text='<%# Convert.ToDateTime(Eval("Date")).ToString("d") %>' meta:resourcekey="dateLabelResource1"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Location" meta:resourcekey="TemplateFieldResource2">
                        <ItemTemplate>
                            <asp:Label runat="server" ID="locationLabel" Text='<%# Eval("Location.Name") %>' meta:resourcekey="locationLabelResource1"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </div>
    </div>
    <script type="text/javascript">
        function onSizeValidate(oSrc, args) {
            var fileinput = document.getElementById("meetingsUpload");
            var ext = args.Value.substring(args.Value.lastIndexOf('.') + 1).toLowerCase();
            args.IsValid = ext == "csv" && checkFileSize(fileinput, 1) ;
        }
        function processForm(e) {
            if (e.preventDefault) e.preventDefault();

            return Page_ClientValidate();
        }
    </script>
</asp:Content>
